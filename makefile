ARM = arm-none-eabi-
CC = $(ARM)gcc -mcpu=arm7tdmi
LD = $(ARM)ld
ALL_FILES = p2test.o scheduler.o initial.o interrupts.o exceptions.o pcb.o asl.o
LIB = /usr/include/uarm/crtso.o /usr/include/uarm/libuarm.o
CFLAGS = -Wall -c -I /usr/include/uarm

all : jaeos16.phase2

jaeos16.phase2 : phase2.elf
	elf2uarm -k $<
	rm -f *.o phase2.elf

phase2.elf :  $(ALL_FILES)
	$(LD) -T /usr/include/uarm/ldscripts/elf32ltsarm.h.uarmcore.x -o phase2.elf $(LIB) $(ALL_FILES)

p2test.o : core/p2test.c
	$(CC) $(CFLAGS) core/p2test.c

scheduler.o : core/scheduler.c
	$(CC) $(CFLAGS) core/scheduler.c

initial.o : core/initial.c
	$(CC) $(CFLAGS) core/initial.c

interrupts.o : core/interrupts.c
	$(CC) $(CFLAGS) core/interrupts.c

exceptions.o : core/exceptions.c
	$(CC) $(CFLAGS) core/exceptions.c

jaeos16.phase1 : phase1.elf
	elf2uarm -k $<
# Removes all *.o file and phase1.elf
	rm -f *.o phase1.elf

phase1.elf : p1test.o pcb.o asl.o
	$(LD) -T /usr/include/uarm/ldscripts/elf32ltsarm.h.uarmcore.x -o phase1.elf $(LIB) p1test.o pcb.o asl.o

p1test.o : core/p1test.c
	$(CC) $(CFLAGS) core/p1test.c

pcb.o : core/pcb.c
	$(CC) $(CFLAGS) core/pcb.c

asl.o : core/asl.c
	$(CC) $(CFLAGS) core/asl.c
