# JaeOS16
- Phase 1 - Level 2: The Queues Manager

- Phase 2 - Level 3: The Nucleus

This project, licensed under the terms of the GNU GPL v2

For license info, see the file "docs/LICENSE.md".

For authors info, see the file "docs/CONTRIBUTORS.md".

For install info, see the file "docs/INSTALL.md".

Copyright (C) 2016 Stefano Giurgiano
