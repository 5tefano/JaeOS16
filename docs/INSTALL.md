#ENG version:
 1- Extract the archive contents into a folder of your choise (ex. "$home/uARM").
 2- Open a terminal, move to the folder chosen in point 1 (ex. cd uARM).
 3- Now launch "make" command.
 4- Open uARM by writing "uARM" on the terminal and click on "machine config" (screwdriver-wrench icon).
 5- Move to "Core file" and click on "browse", then select "phase1.elf.core.uarm" into the folder chosen by you in point 1 and click on "OK".
 6- Turn on the machine by clicking power button, then click on play button. Now click on "Terminals" and then "Terminal 0".
 7- Enjoy! :)

#IT version:
 1- Estrai il contenuto dell'archivio in una qualsiasi cartella a tua scelta (ad esempio "$home/UARM").
 2- Apri il terminale e spostati nella cartella che hai scelto al punto 1 (esempio cd UARM).
 3- Ora lanciate il comando "make".
 4- Apri il programma uARM e clicca su "machine config" (icona cacciavite-chiave inglese).
 5- Spostati su "Core file" e clicca su "sfoglia", seleziona il file "phase1.elf.core.uarm" nella cartella decisa al punto 1 e clicca OK.
 6- Accendi la macchina e premi il tasto play. Ora non ti resta che visualizzare i progressi cliccando su "Terminals" e poi "Terminal 0".
 7- Divertiti! :)
