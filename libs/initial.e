/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef INITIAL_E
#define INITIAL_E

extern pcb_t *currentProcess;       // Processo corrente
extern int processCount;            // Contatore dei processi
extern struct clist readyQueue;     // Coda dei processi pronti
extern int softBlockCount;          // Contatore dei processi bloccati su I/O
extern int semPseudoClock;			// Semaforo dello pseudo-clock

#endif
