/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

void copyState(state_t *state, state_t *copy);

unsigned int stateIsEmpty(state_t *state);

pcb_t* semV(int *semaddr, int weight);

void semP(int *semaddr, int weight, unsigned int io);

void setCommand(unsigned int command, unsigned int *regCommand);

void sysBpHandler(void);

void tlbHandler(void);

void trapHandler(void);

#endif
