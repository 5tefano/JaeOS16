/*
Copyright (C) 2016 Stefano Giurgiano

This file is part of JaeOS16.
JaeOS16 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

JaeOS16 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with JaeOS16.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef _PCB_H
#define _PCB_H

#include "types.h"

void freePcb(pcb_t *);

pcb_t *allocPcb(void);

void initPcbs(void);

void insertProcQ(struct clist *, pcb_t *);

pcb_t *headProcQ(struct clist *);

pcb_t *removeProcQ(struct clist *);

pcb_t *outProcQ(struct clist *, pcb_t *);

int emptyChild(pcb_t *);

void insertChild(pcb_t *, pcb_t *);

pcb_t *removeChild(pcb_t *);

pcb_t *outChild(pcb_t *p);

#endif
