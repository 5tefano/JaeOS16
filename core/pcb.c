/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16.phase1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with uArm.phase1. If not, see <http://www.gnu.org/licenses/>
 */

#include <uARMconst.h>
#include <uARMtypes.h>
#include "../libs/const.h"
#include "../libs/clist.h"
#include "../libs/pcb.h"

HIDDEN struct clist pcbFree = CLIST_INIT;	// coda della lista PCBfree

/**
* freePcb - Inserisce l'elemento puntato da p nella lista pcbFree.
* @p : PCB da aggiungere
*/
void freePcb(pcb_t *p){
	if(p != NULL){
		clist_enqueue(p, &pcbFree, p_list);
	}
}

/**
 *	setEmptyState - Setta tutti campi a 0 di un determinato stato
 *	@state : stato a cui settare i campi vuoti
 */
void setEmptyState(state_t* state){
	state->a1 = 0;
    state->a2 = 0;
    state->a3 = 0;
    state->a4 = 0;
    state->v1 = 0;
    state->v2 = 0;
    state->v3 = 0;
    state->v4 = 0;
    state->v5 = 0;
    state->v6 = 0;
    state->sl = 0;
    state->fp = 0;
    state->ip = 0;
    state->sp = 0;
    state->lr = 0;
    state->pc = 0;
    state->cpsr = 0;
    state->CP15_Control = 0;
    state->CP15_EntryHi = 0;
    state->CP15_Cause = 0;
    state->TOD_Hi = 0;
    state->TOD_Low = 0;
}

/**
* allocPcb - Ritorna NULL se la lista pcbFree list è vuota. Altrimenti, rimuove un elemento
* dalla lista, e inizializza i valori dei campi di ProcBlk e ritorna il puntatore all'elemento rimosso
*/
pcb_t *allocPcb(void){
	pcb_t *pcb = NULL;
	int i;
	if( ! clist_empty(pcbFree) ){				// se la lista pcbFree non è vuota
		pcb = clist_head(pcb, pcbFree, p_list); // ottengo l'indirizzo dell'elemento in testa
		clist_dequeue(& pcbFree);				// lo elimino l'elemento in testa da pcbFree
		pcb->p_list.next = NULL;				// e inizializzo i campi
		pcb->p_children.next = NULL;
		pcb->p_siblings.next = NULL;
		pcb->p_parent = NULL;
		pcb->p_cursem = NULL;
		pcb->p_pid = 0;
		pcb->sem_res = 0;
		pcb->sem_is_IO = FALSE;
		pcb->p_Ktime = 0;
		pcb->p_Gtime = 0;
		setEmptyState(& pcb->p_s);
		for(i = 0; i < EXCP_COUNT; i++){
			setEmptyState(& pcb->p_excpvec[i]);
		}
	}
	return pcb;
}

/**
* initPcbs - Inizializza la lista pcbFree. Contiene tutti gli elementi dell'array statico pcbTable
*/
void initPcbs(void){
	HIDDEN pcb_t pcbTable[MAXPROC];
	int i;
	for(i=0; i<MAXPROC; i++)
		freePcb(& pcbTable[i] );
}

/**
* insertProcQ - Inserisce il ProcBlk puntato da p nella coda dei processi la cui testa è puntata da q.
* @q: testa della coda dei processi
* @p: PCB da aggiungere
*/
void insertProcQ(struct clist *q, pcb_t *p){
	if (p != NULL && q != NULL){
		clist_enqueue(p, q, p_list);
	}
}

/**
* headProcQ - Ritorna un puntatore al primo ProcBlk della coda dei processi
* a cui punta q. Non rimuove il ProcBlk dalla coda dei processi.
* Ritorna NULL se la coda dei processi è vuota.
* @q: puntatore alla coda dei processi
*/
pcb_t *headProcQ(struct clist *q){
	pcb_t* return_type = NULL;
	return (clist_empty(*q) ) ? NULL : clist_head(return_type, *q, p_list);
}

/**
* removeProcQ - Rimuove il primo elemento dalla coda dei processi puntata da q.
* Ritorna NULL se la coda dei processi è vuota; altrimenti ritorna il puntatore all'elemento rimosso.
* @q: puntatore alla coda dei processi
*/
pcb_t *removeProcQ(struct clist *q){
	pcb_t *rm = NULL;

	if(q != NULL && !clist_empty(*q) ){
		rm = clist_head(rm, *q, p_list);
		clist_dequeue(q);
	}
	return rm;
}
/**
* outProcQ - Rimuove il ProcBlk puntato da p dalla coda dei processi puntata da q.
* In caso di errori ritorna NULL; altrimenti, ritorna p.
* @q: puntatore alla coda dei processi
* @p: elemento p da rimuovere
*/
pcb_t *outProcQ(struct clist *q, pcb_t *p){

	if (q != NULL && p != NULL && ! clist_empty(*q) ){
		if(clist_delete(p, q, p_list) == 0)				// Cerco l'elemento p nella coda q
			return p;									// se lo trovo lo elimino e ritorno p
	}
	return NULL;										// in tutti gli altri casi ritorno NULL
}

/**
* emptyChild - Ritorna TRUE se il ProcBlk puntato da p non ha figli; FALSE altrimenti.
* @p: PCB da controllare
*/
int emptyChild(pcb_t *p){
	return (p != NULL && clist_empty(p->p_children) ) ? TRUE : FALSE;
}

/**
* insertChild - Inserisce il ProcBlk puntato da p nella coda dei figli del ProcBlk puntato da parent.
* @parent: puntatore al genitore PCB
* @p: figlio PCB da inserire
*/
void insertChild(pcb_t *parent, pcb_t *p){
	if (parent != NULL && p != NULL){
		clist_enqueue(p, & parent->p_children, p_siblings)
		p->p_parent = parent;
	}
}

/**
* removeChild - Rimuove il primo figlio del ProcBlk puntato da p.
* Ritorna NULL se p non ha figli; altrimenti ritorna il puntatore all'elemento rimosso.
* @p: PCB genitore
*/
pcb_t *removeChild(pcb_t *p){
	pcb_t *rm_child = NULL;

	if (p != NULL && ! emptyChild(p) ){								// p ha figli
		rm_child = clist_head(rm_child, p->p_children, p_siblings); // ottengo l'indirizzo del primo figlio dalla lista
		clist_dequeue(& p->p_children); 							// lo rimuovo dalla lista
		rm_child->p_siblings.next = NULL;							// e setto i campi del figlio rimosso
		rm_child->p_parent = NULL;
	}
	return rm_child;
}

/**
* outChild - Rimuove il processo figlio puntato da p dalla lista dei figli del processo genitore.
* Se il ProcBlk puntato da p non ha genitore, ritorna NULL; altrimenti ritorna p.
* @p: figlio PCB da rimuovere
*/
pcb_t *outChild(pcb_t *p){

	if(p != NULL){
		if(p->p_parent != NULL){
			pcb_t *parent = p->p_parent;											// ottengo l'indirizzo del PCB genitore
			pcb_t *child = clist_head(child, parent->p_children, p_siblings);	// ottengo l'indirizzo del primo figlio
			if(child == p)														 		// se p è il primo figlio,
				return removeChild(parent);												// richiamo removeChild
			else{																		// altrimenti, lo cerco tra la lista
				if(clist_delete(p, & parent->p_children, p_siblings) == 0){				// se lo trovo lo elimino dalla lista
					p->p_parent = NULL;													// e cancello il collegamento al genitore
					return p;
				}
			}
		}
	}
	return NULL;
}
