/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#include <uARMconst.h>
#include <arch.h>

#include "../libs/pcb.h"
#include "../libs/initial.e"
#include "../libs/asl.h"
#include <libuarm.h>
#include "../libs/scheduler.h"

HIDDEN unsigned int startSliceTOD = 0;             // Inizio time slice, lo 0 indica il primo avvio
HIDDEN int timeSlice;                              // Time slice "rimanente"
HIDDEN int timePseudoClock;                        // Time pseudo-clock "rimanente"
HIDDEN unsigned int startPseudoClockTOD = 0;       // Inizio pseudo-clock, lo 0 indica il primo avvio
HIDDEN unsigned int startKernelTimeTOD = 0;        // Inizio tempo esecuzione codice kernel del processo corrente
HIDDEN unsigned int startGlobalTimeTOD = 0;        // Inizio tempo esecuzione del processo corrente

HIDDEN void resetPseudoClock(void);
HIDDEN void resetTimeSlice(void);
HIDDEN void setTimeSlice(void);
HIDDEN void setPseudoClock(void);

/**
 *  setTimeProcessTOD - Imposta/aggiorna il tempo cpu utilizzato
 *  dal processo in modalità global e kernel
 *  @typeTime : modalità
 */
void setTimeProcessTOD(unsigned int typeTime){
    if(currentProcess == NULL) PANIC();

    if(typeTime == KERNEL_TIME){                                    // Impostazione del tempo kernel del processo
        if(startKernelTimeTOD == 0)                                 // Caso particolare : primo avvio in kernel
            startKernelTimeTOD = getTODLO();                        // Setta tempo d'avvio in modalita kernel
        currentProcess->p_Ktime += getTODLO() - startKernelTimeTOD; // Aggiornamento tempo kernel del processo
        startKernelTimeTOD = getTODLO();
    }
    else if(typeTime == GLOBAL_TIME){                               // Impostazione del tempo globale del processo
        if(startGlobalTimeTOD == 0)                                 // Caso particolare : primo avvio in global
            startGlobalTimeTOD = getTODLO();                        // Setta tempo d'avvio in modalita globale
        currentProcess->p_Gtime += getTODLO() - startGlobalTimeTOD; // Aggiornamento tempo globale del processo
        startGlobalTimeTOD = getTODLO();
    }
}

/**
 *  stopCurrentProcess - Ferma il processo corrente ed eventualmente
 *  viene inserito nella coda dei processi pronti
 *  @insert : TRUE inserisce nella readyQueue, FALSE altrimenti
 */
void stopCurrentProcess(unsigned int insert){
    if(currentProcess != NULL){
        if(insert)
            insertProcQ(&readyQueue, currentProcess);   // Inserimento nella coda dei processi pronti
        setTimeProcessTOD(GLOBAL_TIME);                 // Aggiorno tempo globale del processo
        setTimeProcessTOD(KERNEL_TIME);                 // Aggiorno tempo kernek del processo
        startKernelTimeTOD = startGlobalTimeTOD = 0;    // Reset dei tempi d'avvio
        currentProcess = NULL;                          // Sospensione del processo corrente
    }
}

/**
 *  resetPseudoClock - Resetta lo pseudo-clock
 */
HIDDEN void resetPseudoClock(){
    timePseudoClock = SCHED_PSEUDO_CLOCK;
    startPseudoClockTOD = getTODLO();
}

/**
 *  resetTimeSlice - Resetta il time slice
 */
HIDDEN void resetTimeSlice(){
    timeSlice = SCHED_TIME_SLICE;
    startSliceTOD = getTODLO();
}

/**
 *  getPseudoClock - Ritorna lo pseudo-clock rimanente
 */
int getPseudoClock(){
    return SCHED_PSEUDO_CLOCK - (getTODLO() - startPseudoClockTOD);
}

/**
 *  getTimeSlice - Ritorna il time slice rimanete
 */
int getTimeSlice(){
    return SCHED_TIME_SLICE - (getTODLO() - startSliceTOD);
}

/**
 *  setTimeSlice - Aggiorna il time slice
 */
HIDDEN void setTimeSlice(){
    if(startSliceTOD == 0)          // Caso particolare primo avvio
        startSliceTOD = getTODLO(); // Setto tempo d'avvio time slice
    timeSlice = getTimeSlice();     // Time slice rimanente
    if(timeSlice <= 0){             // Time slice scaduto
        resetTimeSlice();
    }
}

/**
 *  setPseudoClock - Aggiorna lo pseudo-clock
 */
HIDDEN void setPseudoClock(){
    if(startPseudoClockTOD == 0)            // Caso particolare primo avvio
        startPseudoClockTOD = getTODLO();   // Setto tempo d'avvio pseudo-clock
    timePseudoClock = getPseudoClock();     // Tempo pseudo-clock rimanente
    if(timePseudoClock <= 0){               // Tempo scaduto
        resetPseudoClock();
    }
}

void scheduler(){
    setTimeSlice();                                             // Aggiorno Time slice
    setPseudoClock();                                           // Aggiorno tempo pseudo-clock
    setTIMER(MIN(timeSlice, timePseudoClock));                  // Impostazione Timer (il minimo tra i due tempi)

    if(currentProcess == NULL){                                 // Nessun processo in esecuzione

        if(headProcQ(&readyQueue) == NULL){                     // La coda dei processi pronti è vuota

            if(processCount == 0){
                HALT();
            }
            else if(processCount > 0 && softBlockCount == 0){   // Deadlock
                PANIC();
            }
            else if(processCount > 0 && softBlockCount > 0){    // Wait state
                setSTATUS(STATUS_ALL_INT_ENABLE(getSTATUS()));  // Abilitazione Interrupt
                WAIT();
            }
        }
        else{
            currentProcess = removeProcQ(&readyQueue);          // Ritorna e rimuove il primo processo dalla coda dei processi pronti
        }
    }
    setTimeProcessTOD(GLOBAL_TIME);                             // Setta glome time del processo
    LDST(& currentProcess->p_s);                                // Avvio processo
}
