/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16.phase1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#include <uARMconst.h>
#include <uARMtypes.h>
#include "../libs/const.h"
#include "../libs/clist.h"
#include "../libs/pcb.h"
#include "../libs/asl.h"

HIDDEN struct clist aslh;					// coda dei semafori attivi (semd)
HIDDEN struct clist semdFree;				// coda dei semafori inattivi (semdfree)
static struct semd_t semdTable[MAXPROC];

/**
* initASL - Initialize the semdFree list to contain all the elements
* of the array static struct semd_t semdTable[MAXPROC]
* NOTE: This method will be only called once during data structure in
* initialization.
*/
void initASL(void){
	int i;
	aslh.next = NULL;
	semdFree.next = NULL;
	for(i=0; i<MAXPROC; i++)
		clist_push(& semdTable[i], & semdFree, s_link);
}

/**
* headBlocked - Return a pointer to the ProcBlk that is at the head of
* the process queue associated with the semaphore semAdd. Return NULL
* if semAdd is not found on the ASL or if the process queue associated
* with semAdd is empty.
* @semAdd: pointer to the semaphore
*/
pcb_t *headBlocked(int *semAdd){
	struct semd_t *scan, *first, *last;
	void *tmp;

	if(semAdd != NULL){

		first = clist_head(first, aslh, s_link);		// primo semd
		last = clist_tail(last, aslh, s_link);			// ultimo semd

		if( ! clist_empty(aslh) && semAdd >= first->s_semAdd && semAdd <= last->s_semAdd){ // semAdd da cercare è compreso tra il primo e l'ultimo semd

			clist_foreach(scan, & aslh, s_link, tmp){	// cerco il semAdd richiesto
				if(scan->s_semAdd == semAdd)			// se lo trovo
					return headProcQ(& scan->s_procq);	// ritorno la testa associata alla coda di semAdd
			}
		}
	}
	return NULL;
}

/**
* outBlocked - Remove the ProcBlk pointed to by p from the process queue associated
* with p's semaphore on the ASL. If ProcBlk pointed to by p does not
* appear in the process queue associated with p's semaphore, which is an
* error condition, return NULL; otherwise, return p.
* @p: PCB to remove
*/
pcb_t *outBlocked(pcb_t *p){
	struct semd_t *sem;
	pcb_t *rm;

	if(p != NULL){
		if(p->p_cursem != NULL){
			sem = p->p_cursem;
			rm = outProcQ(& sem->s_procq, p);			// rimuovo p dalla coda di semAdd
			rm->p_cursem = NULL;

			if(clist_empty(sem->s_procq) ){				// la coda dei processi di semAdd ora è vuota, then is moved from alsh to semdfree
														// di conseguenza viene spostato da alsh a semdfree
				clist_delete(sem, & aslh, s_link);		// elimino da aslh
				clist_push(sem, & semdFree, s_link);	// inserisco in semdfree
			}
			return rm;
		}
	}
	return NULL;
}

/**
 * insertBlocked - Insert the ProcBlk pointed to by p at the tail of the process queue
 * associated with the semaphore whose physical address is semAdd and set the
 * semaphore address of p to semAdd. If the semaphore is currently not active
 * (i.e. there is no descriptor for it in the ASL), allocate a new descriptor
 * from the semdFree list, insert it in the ASL (at the appropriate position),
 * initialize all of the fields (i.e. set s_semAdd to semAdd, and s_procq), and
 * proceed as above. If a new semaphore descriptor needs to be allocated
 * and the semdFree list is empty, return TRUE. In all other cases return FALSE.
 * @semAdd: pointer to sem
 * @p: PCB to add
*/
int insertBlocked(int *semAdd, pcb_t *p){
	struct semd_t *new, *scan;
	void *tmp;

	if(p != NULL && semAdd != NULL){

		clist_foreach(scan, & aslh, s_link, tmp){	// cerco il semAdd richiesto
			if(scan->s_semAdd == semAdd){			// se lo trovo
				insertProcQ(& scan->s_procq, p); 	// inserisco p nella coda dei processi di semAdd
				p->p_cursem = scan;					// p punta al nuovo semaforo
				return FALSE;
			}
		}

		if( ! clist_empty(semdFree) ){					// se semdFree non è vuota
			new = clist_head(scan, semdFree, s_link);	// prendo un nuovo descrittore (il primo) dalla lista semFree
			clist_dequeue(& semdFree);					// e lo cancello da tale lista
			new->s_semAdd = semAdd;						// setto i paramentri del nuovo descrittore
			new->s_procq.next = NULL;
			insertProcQ(& new->s_procq, p);				// e inserisco p nella coda del nuovo descrittore
			p->p_cursem = new;							// p punta al nuovo semaforo

			clist_foreach(scan, & aslh, s_link, tmp){	// successivamente inserisco il nuovo escrittore nella lista aslh
				if(new->s_semAdd < scan->s_semAdd){		// in ordine ascendente
					clist_foreach_add(new, scan, & aslh, s_link, tmp);
					break;
				}
			}
			if(clist_foreach_all(scan, & aslh, s_link,tmp)){ // CASO in cui è maggiore di tutti gli altri elementi
				clist_enqueue(new, & aslh, s_link);		// fine inserimento nella lista alsh
			}
			return FALSE;

		}
		else											// semdFree è vuota
			return TRUE;
	}
	else
		return FALSE;
}

/**
 * removeBlocked - Search the ASL for a descriptor of this semaphore. If none is found, return
 * NULL; otherwise, remove the first (i.e. head) ProcBlk from the process
 * queue of the found semaphore descriptor and return a pointer to it. If the
 * process queue for this semaphore becomes empty remove the semaphore
 * descriptor from the ASL and return it to the semdFree list.
 * @semAdd: pointer to sem
*/
pcb_t *removeBlocked(int *semAdd){
	struct semd_t *scan;
	void *tmp;
	pcb_t *rm;

	if(semAdd != NULL){
		clist_foreach(scan, & aslh, s_link, tmp){ 			// cerco semAdd richiesto
			if(scan->s_semAdd == semAdd){				// se lo trovo
				rm = removeProcQ(& (scan->s_procq));	// rimuovo il primo processo dalla lista associata al semAdd
				rm->p_cursem = NULL;					// resetto il puntatore al semaforo del processo
				if(clist_empty(scan->s_procq) ){		// se ora la lista di semAdd è vuota
					clist_delete(scan, & aslh, s_link);	// lo sposto nella lista dei semdFree
					clist_push(scan, & semdFree, s_link);
				}
				return rm;
			}
		}
	}
	return NULL;
}
