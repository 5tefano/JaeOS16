/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#include "../libs/pcb.h"
#include "../libs/asl.h"
#include <libuarm.h>

#include "../libs/initial.h"
#include "../libs/initial.e"
#include "../libs/exceptions.h"
#include "../libs/interrupts.h"
#include "../libs/scheduler.h"

#define STATE_INT_OLDAREA ((state_t *) INT_OLDAREA)
#define STATE_INT_NEWAREA ((state_t *) INT_NEWAREA)
#define CHECK_BITMAP(bitmap, dev) ((bitmap & dev) == dev)
#define CHECK_TERM(status, type) ((status & DEV_TERM_STATUS) == type)

HIDDEN int getDeviceHighestPriority(unsigned int intLine);
HIDDEN void intDevice(unsigned int intLine);
HIDDEN unsigned int getIntLineCauseIp(unsigned int cause);

/**
 *  getIntLineCauseIp - In ordine di priorità, ottiene la linea che ha generato l'interrupt
 */
HIDDEN unsigned int getIntLineCauseIp(unsigned int cause){
    unsigned int intLine;
    for(intLine = IL_TIMER; !CAUSE_IP_GET(cause, intLine) && intLine <= IL_TERMINAL; intLine++);
    return intLine;
}

/**
 *  getDeviceHighestPriority - Ritorna il numero del dispositivo con la più alta
 *  priorità su una determinata linea di interrupt
 *  @intLine : linea interrupt
 */
HIDDEN int getDeviceHighestPriority(unsigned int intLine){
    memaddr *bitmap, mask, lastBit;
    unsigned int dev;
    if(intLine < IL_TIMER || intLine > IL_TERMINAL) PANIC();            // IL non gestibile, errore

    lastBit = 1 << N_DEV_PER_IL;                                        // Ultimo bit di accesso al bitmap dei dispositivi
    bitmap = (memaddr*) CDEV_BITMAP_ADDR(intLine);                      // Indirizzo del Bitmap relativo all'IL

    for(mask = 1, dev = 0; mask < lastBit && !CHECK_BITMAP(*bitmap, mask); mask = mask << 1, dev++); // Trova il primo dispositivo con richiesta pendente
    if(mask >= lastBit) PANIC();
    return dev;
}

/**
 *  intDevice - Gestore Interrupt Line Device
 */
HIDDEN void intDevice(unsigned int intLine){
    unsigned int device, *regCommand, status;
    dtpreg_t *dtp;
    termreg_t *term;
    int* sem;
    pcb_t *p;

    if(intLine == IL_TIMER){                                    // Gestione del Timer

        if(getPseudoClock() <= 0){                              // Tempo pseudo-clock scaduto
            while(semPseudoClock < 0)                           // Se il semaforo dello pseudo-clock è negativo
                semV(&semPseudoClock, 1);                       // Sblocco tutti i processi bloccati su tale semaforo
            semPseudoClock = 0;                                 // Resetto semaforo dello pseudo-clock
        }
        if(getTimeSlice() <= 0){                                // Time slice scaduto
            stopCurrentProcess(TRUE);                           // Avanti un altro processo
        }

    }
    else if(intLine <= IL_TERMINAL){                            // Gestione Interrupt Line Device generico

        device = getDeviceHighestPriority(intLine);             // Numero del dispositivo con richiesta pendente
        sem = getSemDevice(intLine, device);                    // Accesso al semaforo del dispositivo

        if(intLine < IL_TERMINAL){                              // Interrupt Line (DISK, TAPE, ETHERNET, PRINTER)

            dtp = (dtpreg_t*) DEV_REG_ADDR(intLine, device);    // Ottengo il registro del dispositivo, e relativi valori dei campi
            regCommand = & dtp->command;
            status = dtp->status;
        }
        else{                                                   // Interrupt Line (TERMINAL)

            term = (termreg_t*) DEV_REG_ADDR(intLine, device);  // Ottengo il registro del dispositivo
            if(CHECK_TERM(term->transm_status, DEV_TTRS_S_CHARTRSM)){   // Scrittura (Priorità più elevata)
                regCommand = & term->transm_command;
                status = term->transm_status;
            }
            else if(CHECK_TERM(term->recv_status, DEV_TRCV_S_CHARRECV)){    // Lettura
                sem = getSemDevice(intLine + 1, device);    // Caso particolare : Semaforo terminale di lettura
                regCommand = & term->recv_command;
                status = term->recv_status;
            }
        }

        if(*sem < 1){                       // Se il semaforo del dispositivo è inferiore a 1
            p = semV(sem, 1);               // Sblocco il primo processo bloccato su tale semaforo
            if(p != NULL)
                p->p_s.a1 = status;         // Salvo lo status del dispositivo
        }
        setCommand(DEV_C_ACK, regCommand);  // Invio ACK al dispositivo
    }
}

/**
 *  intHandler - Gestore degli interrupt
 */
void intHandler(){
    unsigned int cause;
    cause = STATE_INT_OLDAREA->CP15_Cause;                      // Ottengo la causa dell'interrupt
    STATE_INT_OLDAREA->pc -= WORD_SIZE;                         // Setto pc per tornare a prima della gestione dell'interrupt
    if(currentProcess != NULL){
        copyState(STATE_INT_OLDAREA, & currentProcess->p_s);
    }
    intDevice( getIntLineCauseIp(cause) );                      // Gestione causa interrupt/dispositivo
    scheduler();
}
