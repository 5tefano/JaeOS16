/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#include "../libs/pcb.h"
#include "../libs/asl.h"
#include <uARMconst.h>
#include <libuarm.h>
#include <arch.h>
#include "../libs/interrupts.h"
#include "../libs/exceptions.h"
#include "../libs/initial.h"
#include "../libs/scheduler.h"

pcb_t *currentProcess;          		        // Processo corrente
int processCount;               			    // Contatore dei processi
struct clist readyQueue = CLIST_INIT;           // Coda dei processi pronti
int softBlockCount;             			    // Contatore dei processi bloccati su I/O
int semPseudoClock;							    // Semaforo dello pseudo-clock
HIDDEN int semDev[N_DEV_TYPES][DEV_PER_INT];    // Struttura dei semafori dei dispositivi
HIDDEN pcb_t * pidProcess[MAXPROC];             // Tabella di assegnamento pid

HIDDEN void initFourAreas();
HIDDEN void initArea(memaddr area, memaddr handler);
HIDDEN void initNucleus();
HIDDEN void initSemaphores();
HIDDEN void instantiateProcess();
extern void test();

/**
 * initFourAreas - Inizializza le quattro aree INT_NEWAREA, TLB_NEWAREA, PGMTRAP_NEWAREA, SYSBK_NEWAREA.
 */
HIDDEN void initFourAreas(){
    initArea(INT_NEWAREA, (memaddr) intHandler);
    initArea(TLB_NEWAREA, (memaddr) tlbHandler);
    initArea(PGMTRAP_NEWAREA, (memaddr) trapHandler);
    initArea(SYSBK_NEWAREA, (memaddr) sysBpHandler);
}

/**
 * initArea - Inizializza una determinata area
 * @area : area da inizializzare
 * @handler : funzione da assegnare all'area
 */
HIDDEN void initArea(memaddr area, memaddr handler){
    state_t *newArea;                                                       // Nuova area da popolare
    if(area == 0 || handler == 0)   PANIC();                                // Parametri errati, errore

    newArea = (state_t *) area;                                             // Semplice cast
    STST(newArea);                                                          // Salva lo stato corrente del processore
    newArea->pc = handler;     	                                            // Setto il PC all'indirizzo della funzione che gestirà l'eccezione
    newArea->sp = RAM_TOP;                                                      // Setto lo SP a RAM_TOP
    newArea->cpsr = STATUS_ALL_INT_DISABLE(newArea->cpsr) | STATUS_SYS_MODE;    // Interrupt mascherati e modalità kernel attivata
}

/**
 * initNucleus - Inizializza le variabili globali processCount, softBlockCount,
 * readyQueue, currentProcess e pidProcess
 */
HIDDEN void initNucleus(){
    int i;
    for(i = 0; i < MAXPROC; i++)
        pidProcess[i] = NULL;
    currentProcess = NULL;
    processCount = softBlockCount = 0;
}

/**
 * initSemaphores - Inizializza i semafori
 */
HIDDEN void initSemaphores(){
    int i, k;
    semPseudoClock = 0;
    for(i = 0; i < N_DEV_TYPES; i++)
		for(k = 0; k < DEV_PER_INT; k++)
			semDev[i][k] = 0;
}

/**
 * instantiateProcess - Instanzia il primo processo all'avvio
 */
HIDDEN void instantiateProcess(){
    pcb_t *firtsProcess = allocPcb();                                                           // instanzia il primo processo
    if(firtsProcess == NULL)    PANIC();                                                        // Nessun pcb libero, errore

    firtsProcess->p_s.cpsr = STATUS_ALL_INT_ENABLE(firtsProcess->p_s.cpsr) | STATUS_SYS_MODE;   // abilita gli interrupt e modalità kernel
    firtsProcess->p_s.CP15_Control = CP15_DISABLE_VM(firtsProcess->p_s.CP15_Control);           // memoria virtuale spenta
    firtsProcess->p_s.sp = RAM_TOP - FRAMESIZE;                                                 // SP punta a RAM_TOP - FRAMESIZE
    firtsProcess->p_s.pc = (memaddr) test;                                                      // PC punta alla funzione test()
    setPidProcess(firtsProcess);						                                        // Setto il pid del processo
    processCount++;                                                                             // Incremento del contatore dei processi
    insertProcQ(&readyQueue, firtsProcess);                                                     // Inserimento nella coda dei processi pronti
}

/**
 *  errorRangePid - Effettua un controllo sul valore del pid
 */
void errorRangePid(pid_t pid){
    if(pid < 0 || pid > MAXPROC)    PANIC();
}

/**
 * setPidProcess - Assegna il pid (con range: da 1 a 20) ad un determinato processo
 * @p : processo su cui lavorare
 */
void setPidProcess(pcb_t *p){
    unsigned int pid;
    for(pid = 0; pid < MAXPROC && pidProcess[pid] != NULL; pid++);
    pid++;
    errorRangePid(pid);
    pidProcess[pid-1] = p;
    p->p_pid = pid;
}

/**
 *  freePidProcess - Rende utilizzabile un determinato pid
 *  @pid : pid da liberare
 */
void freePidProcess(pid_t pid){
    errorRangePid(pid);
    pidProcess[pid - 1] = NULL;
}

/**
 *  findProcess - Ritorna il pcb del processo con tale pid
 *  @pid : pid del processo da trovare
 */
pcb_t* findProcess(pid_t pid){
    errorRangePid(pid);
    return pidProcess[pid - 1];
}

/**
 *  getSemDevice - Ritorna il semaforo associato ad un dispositivo
 *  che giace su una determina linea di interrupt
 */
int* getSemDevice(unsigned int intLine, unsigned device){
    return &semDev[intLine - DEV_IL_START][device];
}

int main(int argc, char const *argv[]) {
    initFourAreas();        // Punto 1
    initPcbs();initASL();   // Punto 2
    initNucleus();          // Punto 3
    initSemaphores();       // Punto 4
    instantiateProcess();   // Punto 5
    scheduler();            // Punto 6
    return 0;
}
