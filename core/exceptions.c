/*
 * Copyright (C) 2016 Stefano Giurgiano
 * This file is part of JaeOS16.
 * JaeOS16 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * JaeOS16 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with JaeOS16. If not, see <http://www.gnu.org/licenses/>
 */

#include <uARMconst.h>
#include <arch.h>

#include "../libs/pcb.h"
#include "../libs/initial.e"
#include "../libs/asl.h"
#include "../libs/initial.h"
#include "../libs/exceptions.h"
#include "../libs/interrupts.h"
#include <libuarm.h>
#include "../libs/scheduler.h"

#define STATE_SYSBK_OLDAREA ((state_t *) SYSBK_OLDAREA)
#define STATE_SYSBK_NEWAREA ((state_t *) SYSBK_NEWAREA)
#define STATE_PGMTRAP_OLDAREA ((state_t *) PGMTRAP_OLDAREA)
#define STATE_PGMTRAP_NEWAREA ((state_t *) PGMTRAP_NEWAREA)
#define STATE_TLB_OLDAREA ((state_t *) TLB_OLDAREA)
#define STATE_TLB_NEWAREA ((state_t *) TLB_NEWAREA)
#define CHECK_STATUS_MODE(status, mode)	((status & mode) == mode)

HIDDEN void handlerUp(unsigned int typeHandler);
HIDDEN void exitFromTrap(unsigned int excptype, unsigned int retval);
HIDDEN void getCpuTime(cputime_t *global, cputime_t *user);
HIDDEN void waitForClock();
HIDDEN void ioDeviceOperation(unsigned int command, int intlNo, unsigned int dnum);
HIDDEN void getProcessId();
HIDDEN void semaphoreOperation(int *semaddr, int weight);
HIDDEN void updateSemaphore(pcb_t *p);
extern int abs(int);

/**
* copyState - Copia un determinato stato
* @state : stato da copiare
* @copy : stato in cui effettuare la copia
*/
void copyState(state_t *state, state_t *copy){
    if(state == NULL || copy == NULL)   PANIC();
    copy->a1 = state->a1;
    copy->a2 = state->a2;
    copy->a3 = state->a3;
    copy->a4 = state->a4;
    copy->v1 = state->v1;
    copy->v2 = state->v2;
    copy->v3 = state->v3;
    copy->v4 = state->v4;
    copy->v5 = state->v5;
    copy->v6 = state->v6;
    copy->sl = state->sl;
    copy->fp = state->fp;
    copy->ip = state->ip;
    copy->sp = state->sp;
    copy->lr = state->lr;
    copy->pc = state->pc;
    copy->cpsr = state->cpsr;
    copy->CP15_Control = state->CP15_Control;
    copy->CP15_EntryHi = state->CP15_EntryHi;
    copy->CP15_Cause = state->CP15_Cause;
    copy->TOD_Hi = state->TOD_Hi;
    copy->TOD_Low = state->TOD_Low;
}

/**
 *  stateIsEmpty - Restituisce TRUE se tutti i campi dello stato sono vuoti (0), FALSE altrimenti
 */
unsigned int stateIsEmpty(state_t *state){
    if(state == NULL)   PANIC();
    return ! (state->a1 | state->a2 | state->a3 | state->a4 | state->v1 |
         state->v2 | state->v3 |state->v4 | state->v5 | state->v6 | state->sl |
         state->fp | state->ip | state->sp | state->lr | state->pc | state->cpsr |
         state->CP15_Control | state->CP15_EntryHi | state->CP15_Cause |
         state->TOD_Hi | state->TOD_Low);
}

/**
 *  createProcess (SYS1) - Crea un nuovo processo e lo inserisce nella coda dei processi pronti
 *  @statep : indirizzo dello stato del processo
 */
HIDDEN void createProcess(state_t *statep){
    pcb_t *newProcess;
    newProcess = (statep != NULL) ? allocPcb() : NULL;
    currentProcess->p_s.a1 = -1;                    // Ritorna -1 in caso di errori

    if(newProcess != NULL){
        copyState(statep, &(newProcess->p_s));      // Viene copiato la stato attuale del processore nello stato del nuovo processo
        insertChild(currentProcess, newProcess);    // Il nuovo processo diventa figlio del processo corrente
        insertProcQ(&readyQueue, newProcess);       // viene inserito nella coda dei processi pronti
        setPidProcess(newProcess);                  // Assegna PID al nuovo processo
        processCount++;
        currentProcess->p_s.a1 = newProcess->p_pid; // Ritorna il PID del processo creato
    }
}

/**
 *  terminateProcess (SYS2) - Termina il processo corrente o un determinato processo e tutta la sua progenie
 *  @pid : pid del processo da terminare
 */
HIDDEN void terminateProcess(pid_t pid){
    pcb_t *p, *child;

    if(pid != 0){
        p = findProcess(pid);                           // Ricerca del processo con tale pid
        if(p != NULL){                                  // Processo da terminare esistente

            while((child = removeChild(p)) != NULL)     // Terminata tutti i processi figli
                terminateProcess(child->p_pid);

            updateSemaphore(p);                         // Aggiorna i semofori associati al processo

            if(p == currentProcess)                     // Terminare processo corrente
                stopCurrentProcess(FALSE);

            outProcQ(&readyQueue, p);                   // Rimuove il processo dalla coda dei processi pronti
            freePidProcess(p->p_pid);                   // Libera il pid associato al processo
            outChild(p);                                // Alla fine, rimuove se stesso
            freePcb(p);                                 // Libera il pcb
            processCount--;                             // Decrementa il contatore dei processi
        }
    }
    else{                                               // (pid == 0) Termina il processo corrente
        if(currentProcess == NULL)  PANIC();
        terminateProcess(currentProcess->p_pid);
    }
}

/**
 *  updateSemaphore - Aggiorna i rispettivi semofori di un processo (in terminazione)
 *  @p : processo su cui aggiornare i semofori
 */
HIDDEN void updateSemaphore(pcb_t *p){
    int *sem;
    if(p == NULL)   PANIC();

    if(p->p_cursem != NULL){                         // Il processo punta ad un semaforo
        sem = p->p_cursem->s_semAdd;
        if(p->sem_is_IO == TRUE)                     // Il semaforo è di IO
            softBlockCount--;                        // Decrementa il contatore dei processi bloccati su IO
        else if(*sem < 0)                            // Il semaforo non è di IO
            (*sem) = (*sem) + p->sem_res;            // Incrementa il valore del semaforo

        if(outBlocked(p) == NULL) PANIC();           // Rimozione del processo dal semaforo associato
    }
}

/**
 *  semaphoreOperation (SYS3)- Effettua l'operazione di V o P sul semaforo
 *  @semaddr : indirizzo del semaforo
 *  @weight : operazione da effettuare
 */
HIDDEN void semaphoreOperation(int *semaddr, int weight){
    if(semaddr == NULL) PANIC();

    if(weight > 0){
        semV(semaddr, weight);
    }
    else if(weight < 0){
        semP(semaddr, weight, FALSE);
    }
    else
        terminateProcess(0);
}

/**
 *  semP - Attende il verificarsi di un evento o il rilascio di una risorsa
 *  @semaddr : indirizzo del semaforo
 *  @weight : numero di risorse bloccate
 *  @io : indica se il semoforo è di tipo input/output
 */
void semP(int *semaddr, int weight, unsigned int io){
    if(semaddr == NULL) PANIC();
    weight = abs(weight);
    (*semaddr) -= weight;                                    // Decremento del valore del semaforo
    if(*semaddr < 0){                                        // Il semoforo è negativo

        if(insertBlocked(semaddr, currentProcess))  PANIC(); // Inserimento del processo in coda al semaforo
        currentProcess->sem_res = weight;                    // Assegnamento risorse richieste dal processo
        if(io == TRUE){                                      // Semaforo di tipo Input/output
            currentProcess->sem_is_IO = TRUE;                // Settaggio del semoforo del processo in IO
            softBlockCount++;                                // Incremento del contatore dei processi bloccati su IO
        }
        stopCurrentProcess(FALSE);                           // Sospensione processo corrente (senza inserimento nella readyQueue)
    }
}

/**
 *  semV - Invia un segnale per indicare il verificarsi di un evento
 *  o il rilascio di una risorsa; Inoltre ritorna un processo sbloccato
 *  @semaddr : indirizzo del semaforo
 *  @weight : numero di risorse liberate
 */
pcb_t* semV(int *semaddr, int weight){
    pcb_t *process;
    if(semaddr == NULL) PANIC();

    (*semaddr) += weight;                           // Incremento del valore del semaforo
    process = headBlocked(semaddr);                 // Primo processo in testa la semaforo
    if(process != NULL){
        process->sem_res -= weight;                 // Decremento le risorse richieste del processo

        if(process->sem_res <= 0){                  // Se la richiesta è soddisfatta

            if(process == removeBlocked(semaddr)){  // Risveglio del processo in testa alla coda del semaforo

                if(process->sem_is_IO == TRUE){     // Il semoforo del processo risvegliato è di tipo IO
                    softBlockCount--;               // Decremento del contatore dei processi bloccati su IO
                    process->sem_is_IO = FALSE;
                }
                insertProcQ(&readyQueue, process);  // Inserisco il processo nella coda dei processi pronti
            }
        }
    }
    return process;
}

/**
 *  specifyHandler - Setta le aree (sys/bp, trap, tld) del rispettivo processo
 *  @sys : tipo di area da popolare
 *  @handler : indirizzo del gestore dell'eccezzione
 *  @stack : indirizzo stack gestore
 *  @execution : modalità del gestore
 */
HIDDEN void specifyHandler(unsigned int sys, unsigned int handler, unsigned int stack, unsigned int execution){
    unsigned int excpNew;
    if(currentProcess == NULL)  PANIC();

    switch(sys){    // Ottiene il numero della nuova area da popolare in base al tipo
        case SPECSYSHDL:
            excpNew = EXCP_SYS_NEW; break;
        case SPECTLBHDL:
            excpNew = EXCP_TLB_NEW; break;
        case SPECPGMTHDL:
            excpNew = EXCP_PGMT_NEW;
    }

    if(stateIsEmpty(& currentProcess->p_excpvec[excpNew])){                         // La richiesta è stata eseguita una volta sola
        currentProcess->p_excpvec[excpNew].pc = handler;                            // Setta PC all'indirizzo del gestore
        currentProcess->p_excpvec[excpNew].sp = stack;                              // Setta SP all'indirizzo dello stack del gestore
        currentProcess->p_excpvec[excpNew].cpsr = execution & 0x000000FF;           // Copia i primi otto bit
        currentProcess->p_excpvec[excpNew].CP15_Control = execution & 0x80000000;   // Setta la memoria virtuale in base al 31esimo bit di execution
    }
    else{                                                                           // La richiesta è gia stata eseguita
        terminateProcess(0);                                                        // Termina processo corrente
    }
}

/**
 *  handlerUp - Gestore di livello superiore
 *  @typeHandler : tipo di richiesta da gestire
 */
HIDDEN void handlerUp(unsigned int typeHandler){
    unsigned int excpOld, excpNew;
    state_t *stateOldArea;
    if(currentProcess == NULL) PANIC();

    switch(typeHandler){                                        // In base al tipo di richiesta da gestire, vengono inizializzate le varibili
        case SPECSYSHDL:
            stateOldArea = STATE_SYSBK_OLDAREA;
            excpOld = EXCP_SYS_OLD;
            excpNew = EXCP_SYS_NEW;
            break;
        case SPECTLBHDL:
            stateOldArea = STATE_TLB_OLDAREA;
            excpOld = EXCP_TLB_OLD;
            excpNew = EXCP_TLB_NEW;
            break;
        case SPECPGMTHDL:
            stateOldArea = STATE_PGMTRAP_OLDAREA;
            excpOld = EXCP_PGMT_OLD;
            excpNew = EXCP_PGMT_NEW;
            break;
        default:
            PANIC();
    }
    if( stateIsEmpty(& currentProcess->p_excpvec[excpNew]) ){   // Se la nuova area relativa al processo corrente è nuova
        terminateProcess(0);                                    // Termina il processo corrente
        scheduler();
    }
    else{                                                       // Se la nuova area relativa al processo corrente è stata inizializzata
        copyState(stateOldArea, & currentProcess->p_excpvec[excpOld]);
        switch(typeHandler){                                    // Copia il vecchio stato e inizializza i vari campi
            case SPECSYSHDL:
                currentProcess->p_excpvec[excpNew].a1 = stateOldArea->a1;
                currentProcess->p_excpvec[excpNew].a2 = stateOldArea->a2;
                currentProcess->p_excpvec[excpNew].a3 = stateOldArea->a3;
                currentProcess->p_excpvec[excpNew].a4 = stateOldArea->a4;
                currentProcess->p_excpvec[excpNew].a1 |= stateOldArea->cpsr << 28;
                break;
            default: // SPECTLBHDL e SPECPGMTHDL
                currentProcess->p_excpvec[excpNew].a1 = CAUSE_EXCCODE_GET(stateOldArea->CP15_Cause);
        }
        LDST(& currentProcess->p_excpvec[excpNew]);
    }
}

/**
 *  sysBpHandler - Gestore system call e breakpoint
 */
void sysBpHandler(){
    unsigned int status, a1, a2, a3, a4, causeExcCode;
    if(currentProcess == NULL) PANIC();

    copyState(STATE_SYSBK_OLDAREA, & currentProcess->p_s);              // Copia lo stato del processo nell'apposita area
    setTimeProcessTOD(KERNEL_TIME);                                     // Setta/aggiorna il tempo di avvio in modalita kernel del processo
    causeExcCode = CAUSE_EXCCODE_GET(STATE_SYSBK_OLDAREA->CP15_Cause);  // Ottiene il codice della causa
    status = STATE_SYSBK_OLDAREA->cpsr;                                 // Modalità di esecuzione
    a1 = STATE_SYSBK_OLDAREA->a1;                                       // Parametri system call
    a2 = STATE_SYSBK_OLDAREA->a2;
    a3 = STATE_SYSBK_OLDAREA->a3;
    a4 = STATE_SYSBK_OLDAREA->a4;

    if(causeExcCode == EXC_SYSCALL){                                    // Si è verificata una system call

        if(CHECK_STATUS_MODE(status, STATUS_SYS_MODE)){                 // Il processo è in kernel mode

            switch(a1){                                                 // Gestione delle varie system call
                case CREATEPROCESS:
                    createProcess((state_t *) a2);
                    break;
                case TERMINATEPROCESS:
                    terminateProcess((pid_t) a2);
                    break;
                case SEMOP:
                    semaphoreOperation((int *) a2, (int) a3);
                    break;
                case SPECSYSHDL:
                    specifyHandler(a1, a2, a3, a4);
                    break;
                case SPECTLBHDL:
                    specifyHandler(a1, a2, a3, a4);
                    break;
                case SPECPGMTHDL:
                    specifyHandler(a1, a2, a3, a4);
                    break;
                case EXITTRAP:
                    exitFromTrap(a2, a3);
                    break;
                case GETCPUTIME:
                    getCpuTime((cputime_t *) a2, (cputime_t *) a3);
                    break;
                case WAITCLOCK:
                    waitForClock();
                    break;
                case IODEVOP:
                    ioDeviceOperation(a2, (int) a3, a4);
                    break;
                case GETPID:
                    getProcessId();
                    break;
                default:    // LDST, PANIC, PANIC
                    handlerUp(SPECSYSHDL);
            }
            scheduler();
        }
        else if(CHECK_STATUS_MODE(status, STATUS_USER_MODE)){           // Il processo è in user mode

            if(a1 >= CREATEPROCESS && a1 <= SYSCALL_MAX){               // Se è una system call
                copyState(STATE_SYSBK_OLDAREA, STATE_PGMTRAP_OLDAREA);  // Attivo una Trap
                STATE_PGMTRAP_OLDAREA->CP15_Cause = CAUSE_EXCCODE_SET(STATE_PGMTRAP_OLDAREA->CP15_Cause, EXC_RESERVEDINSTR);
                trapHandler();
            }
            else{                                                       // Se non è una system call
                handlerUp(SPECSYSHDL);                                  // Richiamo gestore di livello superiore
            }
        }
        else{
            PANIC();
        }
    }
    else if(causeExcCode == EXC_BREAKPOINT){                            // Si è verificato un breakpoint
        handlerUp(SPECSYSHDL);                                          // Richiamo gestore di livello superiore
    }
    else{                                                               // Si è verificata un'eccezione non prevista
        PANIC();
    }
}

/**
 *  tlbHandler - Gestore Translation Lookaside Buffer
 */
void tlbHandler(){
    handlerUp(SPECTLBHDL);
}

/**
 *  trapHandler - Gestore delle program trap
 */
void trapHandler(){
    handlerUp(SPECPGMTHDL);
}

/**
 *  exitFromTrap (SYS7) - Uscita da un deteminato handler
 *  @excptype : tipo dell'eccezione
 *  @retval : possibile valore di ritorno
 */
HIDDEN void exitFromTrap(unsigned int excptype, unsigned int retval){
    if(currentProcess != NULL){
        currentProcess->p_excpvec[excptype].a1 = retval;
        LDST(& currentProcess->p_excpvec[excptype]);
    }
}

/**
 *  getCpuTime (SYS8) - Ottiene il tempo di esecuzione globale e user del
 *  processo corrente
 */
HIDDEN void getCpuTime(cputime_t *global, cputime_t *user){
    setTimeProcessTOD(GLOBAL_TIME);
    setTimeProcessTOD(KERNEL_TIME);
    *global = currentProcess->p_Gtime;
    *user = currentProcess->p_Gtime - currentProcess->p_Ktime;
}

/**
 *  waitForClock (SYS9) - Mette in attesa il processo corrente sul semaforo
 *  dello pseudo-clock
 */
HIDDEN void waitForClock(){
    semP(& semPseudoClock, -1, TRUE);
}

/**
 *  ioDeviceOperation (SYS10) - Effettua operazioni di I/O sui dispositivi
 *  @command : comando da inviare al dispositivo
 *  @intlNo : numero linea interrupt
 *  @dnum : numero dispositivo
 */
HIDDEN void ioDeviceOperation(unsigned int command, int intlNo, unsigned int dnum){
    unsigned int isTerminalR, intLineSem, *regCommand;
    termreg_t *t;
    dtpreg_t *d;

    isTerminalR = dnum >> 31;                               // Il dispositivo è di tipo read (true), altrimenti false
    intLineSem = (isTerminalR) ? (intlNo + 1) : intlNo;     // Indice della linea di interrupt relativa al semaforo (+1 per terminale di lettura)

    semP(getSemDevice(intLineSem, dnum), -1, TRUE);         // P sul semaforo del dispositivo

    if(intlNo == IL_TERMINAL){                              // Dispositivo di tipo terminale
        t = (termreg_t *) DEV_REG_ADDR(intlNo, dnum);       // Accesso al registro del dispositivo, e relaviti campi
        regCommand = (isTerminalR) ? & t->recv_command : & t->transm_command;
    }
    else{                                                   // Dispositivo di altro genere
        d = (dtpreg_t *) DEV_REG_ADDR(intlNo, dnum);        // Accesso al registro del dispositivo, e relativi campi
        regCommand = & d->command;
    }
    setCommand(command, regCommand);                        // Invia richiesta al dispositivo
}

/**
 *  setCommand - Imposta ed esegue una determina richiesta ad un dispositivo
 *  @command : comando da inviare
 *  @regCommand : indirizzo del campo del dispositivo su cui inviare la richiesta
 */
void setCommand(unsigned int command, unsigned int *regCommand){
    if(regCommand != NULL){
        *regCommand = command;
    }
}

/**
 *  getProcessId (SYS11) - Ritorna il PID del processo corrente
 */
HIDDEN void getProcessId(){
    if(currentProcess != NULL)
        currentProcess->p_s.a1 = currentProcess->p_pid;
}
